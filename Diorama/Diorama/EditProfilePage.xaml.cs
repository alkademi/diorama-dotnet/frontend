﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProfilePage : ContentPage
    {
        public string ProfileName { get; set; }
        public string Biography { get; set; }
        public string Username { get; set; }
        public string ProfilePicture { get; set; }

        private string NewPicture { get; set; } = string.Empty;

        public EditProfilePage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var response = await Utils.HttpGet("/api/user");
            var error = Utils.getErrorMessage(response);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }

            var okobj = Utils.getOkObject(response);

            ProfileName = okobj["name"].ToString();
            Username = okobj["username"].ToString();
            Biography = okobj["biography"].ToString();
            ProfilePicture = okobj["profile_picture"].ToString();

            txtUsername.Text = Username;
            txtName.Text = ProfileName;
            txtBio.Text = Biography;

            var pictureSource = Utils.getImageFromUrl(ProfilePicture);
            profilePicture.Source = pictureSource;
            if (!string.IsNullOrWhiteSpace(NewPicture))
            {
                var newpictureSource = PictureUtils.ConvertB64ToImageSource(NewPicture);
                profilePicture.Source = newpictureSource;
            }
        }

        private async void Apply_Tapped(object sender, EventArgs e)
        {
            ProfileName = txtName.Text;
            Username = txtUsername.Text;
            Biography = txtBio.Text;

            if (!string.IsNullOrWhiteSpace(NewPicture))
            {
                string imgUrl = await PictureUtils.Upload(NewPicture);
                if (string.IsNullOrWhiteSpace(imgUrl))
                {
                    await DisplayAlert("Error", "Image Upload Failed", "Try Again");
                    return;
                }
                ProfilePicture = imgUrl;
            }

            var res = await Utils.HttpPut("/api/user/edit", new { Username = Username, Name = ProfileName, Biography = Biography, ProfilePicture = ProfilePicture });
            var error = Utils.getErrorMessage(res);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }

            await PageUtils.Move(this, new ProfilePage());
        }
        private async void Discard_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }

        private async void Change_Tapped(object sender, EventArgs e)
        {
            Func<bool> onPex = delegate ()
            {
                DisplayAlert("Alert", "Permission problem", "Try Again");
                return true;
            };

            Func<bool> onUex = delegate ()
            {
                DisplayAlert("Alert", "Something went wrong", "Try Again");
                return true;
            };
            var photo = await PictureUtils.PickPhotoAsync(onPex, onUex);
            if (!string.IsNullOrWhiteSpace(photo))
            {
                NewPicture = photo;
                profilePicture.Source = PictureUtils.ConvertB64ToImageSource(photo);
                await profilePicture.ScaleTo(1);
            }
        }
    }
}