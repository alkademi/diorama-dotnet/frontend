﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditPostPage : ContentPage
    {
        private Image Image0;
        private string postID;
        private string capt;
        public EditPostPage(string postID, Image pict, string capt)
        {
            this.postID = postID;
            this.capt = capt;
            InitializeComponent();
            Console.WriteLine(pict);
            Console.WriteLine(capt);
            Image0 = pict;
            ImageViewer.Source = pict.Source;
        }

        private async void ToUploadSecondPart(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new EditSecondPartPage(this.postID, this.capt, Image0));
        }
        private void Image_Tapped(object sender, EventArgs e)
        {
            if (!(Image0 is null))
            {
                Image0.ScaleTo(1);
            }
            var Image = (Image)sender;
            Image.ScaleTo(0.8);
            ImageViewer.Source = Image.Source;
            Image0 = Image;
        }
    }
}