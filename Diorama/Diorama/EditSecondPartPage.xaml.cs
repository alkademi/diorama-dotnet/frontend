﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditSecondPartPage : ContentPage
    {
        private string postID;
        private Image img;
        public EditSecondPartPage(string postID, string capt, Image img)
        {
            InitializeComponent();
            this.postID = postID;
            this.img = img;
            caption.Text = capt;
        }
        private async void Apply_Clicked(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }
    }
}