﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UploadSecondPartPage : ContentPage
    {
        private string PhotoRaw { get; set; }
        public UploadSecondPartPage(string photoRaw)
        {
            InitializeComponent();
            PhotoRaw = photoRaw;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            ImageViewer.Source = PictureUtils.ConvertB64ToImageSource(PhotoRaw);
            await ImageViewer.ScaleTo(1);
        }

        private async void Apply_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Caption.Text))
            {
                await DisplayAlert("Alert", "Please fill the caption", "OK");
                return;
            }

            string imgUrl = await PictureUtils.Upload(PhotoRaw);
            if (string.IsNullOrWhiteSpace(imgUrl))
            {
                await DisplayAlert("Error", "Image Upload Failed", "Try Again");
                return;
            }

            var response = await Utils.HttpPost("/api/post", new { Caption = Caption.Text, Image = imgUrl });
            var error = Utils.getErrorMessage(response);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }
            await PageUtils.Move(this, new HomePage());
        }
        private async void Profile_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }
        private async void Home_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new HomePage());
        }
        private async void Search_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPage());
        }
        private async void Add_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }
    }
}