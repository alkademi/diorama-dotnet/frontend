﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json.Serialization;
using Xamarin.Essentials;
using System.IO;

namespace Diorama
{
    public class PageUtils
    {
        public async static Task Move(NavigableElement nav,Page page)
        {
            await nav.Navigation.PushModalAsync(new NavigationPage(page)
            {
                BarBackgroundColor = Color.RoyalBlue,
                BarTextColor = Color.White,
            });
        }
    }

    public class Storage
    {
        public static bool HasKey(string key)
        {
            return Application.Current.Properties.ContainsKey(key);
        }

        public static object Get(string key)
        {
            return Application.Current.Properties[key];
        }

        public async static Task ClearAll()
        {
            Application.Current.Properties.Clear();
            await Application.Current.SavePropertiesAsync();
        }

        public async static Task Set(string key, object value)
        {
            Application.Current.Properties[key] = value;
            await Application.Current.SavePropertiesAsync();
        }
    }

    public class PictureUtils
    {
        private static readonly string IMGBB_API_KEY = App.IMGBB_API_KEY;
        private static readonly bool USE_IMGBB = true;
        private static readonly string LOCAL_FS_HOST = App.LOCAL_FS_HOST;

        public async static Task<string> Upload(string source)
        {
            var handler = new HttpClientHandler()
            {
                ServerCertificateCustomValidationCallback = delegate { return true; },
            };
            HttpClient client = new HttpClient();
            if (USE_IMGBB)
            {
                var request = new HttpRequestMessage(new HttpMethod("POST"), "https://api.imgbb.com/1/upload?key=" + IMGBB_API_KEY);
                var multipartContent = new MultipartFormDataContent();
                multipartContent.Add(new StringContent(source), "image");
                request.Content = multipartContent;
                try
                {
                    var response = await client.SendAsync(request);
                    var result = await Utils.ConvertResponse(response);
                    Dictionary<string, object> resultObj = (Dictionary<string, object>)result;
                    Dictionary<string, object> data = (Dictionary<string, object>)resultObj["data"];
                    return data["display_url"].ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                return string.Empty;
            }
            return string.Empty;
            //else
            //{
            //    var byteArr = Convert.FromBase64String(source);
            //    var multiForm = new MultipartFormDataContent();

            //    var imgContent = new ByteArrayContent(byteArr);
            //    multiForm.Add(new StreamContent(fs), "image")
            //}
        }

        public static ImageSource ConvertB64ToImageSource(string source)
        {
            var byteArr = Convert.FromBase64String(source);
            return ImageSource.FromStream(() => new MemoryStream(byteArr));
        }
        
        public static async Task<string> PickPhotoAsync(Func<bool> onPermissionError, Func<bool> onUnknownError)
        {
            try
            {
                var photo = await MediaPicker.PickPhotoAsync();
                if (photo == null)
                {
                    return string.Empty;
                }
                return await ReadPhoto(photo);
            }
            catch (PermissionException)
            {
                onPermissionError();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                onUnknownError();
            }
            return string.Empty;
        }

        public static async Task<string> TakePhotoAsync(Func<bool> onPermissionError, Func<bool> onUnknownError)
        {
            try
            {
                var photo = await MediaPicker.CapturePhotoAsync();
                if (photo == null)
                {
                    return string.Empty;
                }
                return await ReadPhoto(photo);
            }
            catch (PermissionException)
            {
                onPermissionError();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                onUnknownError();
            }
            return string.Empty;
        }

        private static async Task<string> ReadPhoto(FileResult photo)
        {
            var stream = await photo.OpenReadAsync();
            var bytes = new byte[(int)stream.Length];
            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(bytes, 0, (int)stream.Length);
            var b64 = Convert.ToBase64String(bytes);
            return b64;
        }
    }
    public class Utils
    {
        private static readonly string BASE_ADDRESS = App.BASE_ADDRESS;
        private static readonly string DEFAULT_IMAGE_URL = "https://i.imgur.com/RfrXKbu.png"; //"https://i.imgur.com/AQE3Gt1.png";
        public static readonly JsonSerializerSettings JSON_SETTINGS;
        static Utils()
        {
            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            };

            JSON_SETTINGS = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
            };
        }

        public static UriImageSource getImageFromUrl(string url)
        {
            string source = url;
            if (url == null || string.IsNullOrWhiteSpace(url))
            {
                source = DEFAULT_IMAGE_URL;
            }
            UriImageSource img = new UriImageSource { Uri = new Uri(source) };
            img.CachingEnabled = true;
            img.CacheValidity = TimeSpan.FromMinutes(30);
            return img;
        }

        public static string getErrorMessage(object response)
        {
            if (!(response is Dictionary<string, object>))
            {
                return string.Empty;
            }

            Dictionary<string, object> data = (Dictionary<string, object>)response;
            if (data.ContainsKey("title"))
            {
                return data["title"].ToString();
            }
            if (data.ContainsKey("status_code"))
            {
                string code = data["status_code"].ToString();
                if (code != "200")
                {
                    return data["details"].ToString();
                }
            }
            return string.Empty;
        }
        public static string getOkMessage(object response)
        {
            if (!(response is Dictionary<string, object>))
            {
                return "Bad Response";
            }
            Dictionary<string, object> data = (Dictionary<string, object>)response;
            if (data.ContainsKey("status_code"))
            {
                string code = data["status_code"].ToString();
                if (code == "200")
                {
                    if (data["details"] is string)
                    {
                        return data["details"].ToString();
                    }
                    return JsonConvert.SerializeObject(data["details"], JSON_SETTINGS);
                }
            }
            return "Bad Response";
        }

        public static Dictionary<string, object> getOkObject(object response)
        {
            if (!(response is Dictionary<string, object>))
            {
                return null;
            }
            Dictionary<string, object> data = (Dictionary<string, object>)response;
            if (data.ContainsKey("status_code"))
            {
                string code = data["status_code"].ToString();
                if (code == "200")
                {
                    if (data["details"] is string)
                    {
                        return null;
                    }
                    return (Dictionary<string, object>)data["details"];
                }
            }
            return null;
        }

        public static string getToken()
        {
            if (!Storage.HasKey("Token"))
            {
                return string.Empty;
            }
            return Storage.Get("Token").ToString();
        }

        private static HttpClient GetClient()
        {
            var handler = new HttpClientHandler()
            {
                ServerCertificateCustomValidationCallback = delegate { return true; },
            };
            HttpClient client = new HttpClient();
            var token = getToken();
            if (!string.IsNullOrEmpty(token))
            {
                client.DefaultRequestHeaders.Add("Authorization", token);
            }
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            return client;
        }
        public static object Normalize(object requestObject)
        {
            switch (requestObject)
            {
                case JObject jObject:
                    return ((IEnumerable<KeyValuePair<string, JToken>>)jObject).ToDictionary(j => j.Key, j => Normalize(j.Value));
                case JArray jArray: 
                    return jArray.Select(Normalize).ToList();
                case JValue jValue: 
                    return jValue.Value;
                default: 
                    throw new Exception($"Unsupported type: {requestObject.GetType()}");
            }
        }

        public async static Task<object> ConvertResponse(HttpResponseMessage msg)
        {
            var result = await msg.Content.ReadAsStringAsync();
            Console.WriteLine(result);
            var ret = Normalize(JsonConvert.DeserializeObject(result));
            var errMsg = getErrorMessage(ret);
            Console.WriteLine(errMsg);
            if (!string.IsNullOrWhiteSpace(errMsg) && errMsg.Equals("Not logged in."))
            {
                await Storage.ClearAll();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
                return ret;
            }
            return ret;
        }

        public async static Task<object> HttpGet(string url)
        {
            HttpClient client = GetClient();
            try
            {
                var response = await client.GetAsync(BASE_ADDRESS + url);
                return await ConvertResponse(response);
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new { };
            }
        }

        public async static Task<object> HttpDelete(string url)
        {
            HttpClient client = GetClient();
            try
            {
                var response = await client.DeleteAsync(BASE_ADDRESS + url);
                return await ConvertResponse(response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new { };
            }
        }

        public async static Task<object> HttpPost(string url, object body)
        {
            HttpClient client = GetClient();
            try
            {
                var response = await client.PostAsync(BASE_ADDRESS + url, new StringContent(JsonConvert.SerializeObject(body, JSON_SETTINGS), Encoding.UTF8, "application/json"));
                return await ConvertResponse(response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new { };
            }
        }
        public async static Task<object> HttpPut(string url, object body)
        {
            HttpClient client = GetClient();
            try
            {
                var response = await client.PutAsync(BASE_ADDRESS + url, new StringContent(JsonConvert.SerializeObject(body, JSON_SETTINGS), Encoding.UTF8, "application/json"));
                return await ConvertResponse(response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new { };
            }
        }
    }
}
