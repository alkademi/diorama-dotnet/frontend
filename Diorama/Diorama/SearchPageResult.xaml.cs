﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    public class SearchItemResult
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string RawUsername { get; set; }

        public UriImageSource Picture { get; set; }

        public SearchItemResult(Dictionary<string, object> data)
        {
            Name = data["name"].ToString();
            RawUsername = data["username"].ToString();
            Username = "@" + RawUsername;
            Picture = Utils.getImageFromUrl(data["profile_picture"].ToString());
        }
        public SearchItemResult() { }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPageResult : ContentPage
    {
        protected override void OnAppearing()
        {
            base.OnAppearing();
            NoResultText.IsVisible = false;
            Search();
        }
        private async void Home_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new HomePage());
        }
        private async void Search_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPage());
        }
        private async void Add_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }
        private async void Profile_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }
        private async void Edit_Button_Clicked(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new EditProfilePage());
        }

        private async void Search()
        {
            var result = await Utils.HttpGet("/api/user/" + SearchBarText.Text + "/search");
            var okobj = Utils.getOkObject(result);
            if (okobj == null)
            {
                NoResultText.IsVisible = true;
                SearchResults.IsVisible = false;
                return;
            }
            var usersobj = (IEnumerable<object>)okobj["users"];
            var users = usersobj.Select(u => (Dictionary<string, object>)u);
            if (users.Count() > 0)
            {
                SearchResults.IsVisible = true;
                NoResultText.IsVisible = false;
                SearchResults.ItemsSource = users.Select(x => new SearchItemResult(x));
            }
            else
            {
                NoResultText.IsVisible = true;
                SearchResults.IsVisible = false;
            }
        }

        public SearchPageResult(string username)
        {
            InitializeComponent();
            BindingContext = this;
            SearchBarText.Text = username;
        }

        private async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            SearchItemResult selItem = (SearchItemResult)e.Item;
            await PageUtils.Move(this, new OtherUserPage(selItem.RawUsername));
            ((ListView)sender).SelectedItem = null;
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            Search();
        }
    }
}
