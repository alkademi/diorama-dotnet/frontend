﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Diorama
{
    public class CommentResult
    {
        public string Username { get; set; }
        public string Comment { get; set; }

        public CommentResult(Dictionary<string, object> data)
        {
            Username = data["from"].ToString();
            Comment = data["content"].ToString();
        }

        public CommentResult(string username, string comment)
        {
            Username = username;
            Comment = comment;
        }

        public CommentResult() { }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpesificPostPage : ContentPage
    {
        private int _postId;
        private string _username;

        ObservableCollection<CommentResult> Comments = new ObservableCollection<CommentResult> { };

        public SpesificPostPage(int postId)
        {
            InitializeComponent();

            _postId = postId;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            RegisterUsername();
            SpesificPostInit();
            CommentsInit();
        }

        private async void RegisterUsername()
        {
            var response = await Utils.HttpGet("/api/user");
            var error = Utils.getErrorMessage(response);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }

            var okobj = Utils.getOkObject(response);

            _username = okobj["username"].ToString();
        }

        private async void SpesificPostInit()
        {
            var response = await Utils.HttpGet($"/api/post/{_postId}");
            var error = Utils.getErrorMessage(response);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }

            var okobj = Utils.getOkObject(response);

            // Setup post caption text.
            captionSpan.Text = okobj["caption"].ToString();

            // Setup post like text.
            likesCount1.Text = okobj["likes"].ToString();

            Dictionary<string, object> author = (Dictionary<string, object>)okobj["author"];
            usernameLabel.Text = author["username"].ToString();

            // Setup post image.
            var pictureSource = new UriImageSource { Uri = new Uri(okobj["image"].ToString()) };
            pictureSource.CachingEnabled = false;
            pictureSource.CacheValidity = TimeSpan.FromHours(1);
            picture.Source = pictureSource;

            // Setup like image.
            var likeStatusResponse = await Utils.HttpGet($"/api/post/{_postId}/like/status");
            var likeStatusError = Utils.getErrorMessage(likeStatusResponse);
            if (!string.IsNullOrEmpty(likeStatusError))
            {
                await DisplayAlert("Error", likeStatusError, "Try Again");
                return;
            }

            var likeStatusObj = Utils.getOkObject(likeStatusResponse);
            bool likeStatusVal = bool.Parse(likeStatusObj["status"].ToString());

            if (likeStatusVal)
            {
                likeImage1.Source = "like1";
            }

            // Setup deleteButton
            deletePostButton.IsVisible = author["username"].ToString() == _username;
        }

        private async void CommentsInit()
        {
            var commentsResponse = await Utils.HttpGet($"/api/post/{_postId}/comment");

            var commentsError = Utils.getErrorMessage(commentsResponse);
            if (!string.IsNullOrEmpty(commentsError))
            {
                await DisplayAlert("Error", commentsError, "Try Again");
                return;
            }

            var commentsOkObject = Utils.getOkObject(commentsResponse);
            var commentsObj = (IEnumerable<object>)commentsOkObject["comments"];
            var comments = commentsObj
                .Select(c => (Dictionary<string, object>)c)
                .Select(c => new CommentResult(c));

            foreach (CommentResult comment in comments)
            {
                Comments.Insert(0, comment);
            }

            if (comments.Count() > 0)
            {
                CommentResults.ItemsSource = Comments;
            }
        }

        private async void Add_Comment(object sender, EventArgs e)
        {
            if (_username != "")
            {
                string commentContent = commentsInput.Text;
                await Utils.HttpPost($"/api/post/{_postId}/comment", new { Content = commentContent });
                Comments.Insert(0, new CommentResult(_username, commentContent));
            }
        }

        private async void Home_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new HomePage());
        }
        private async void Search_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPage());
        }
        private async void Add_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }
        private async void Profile_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }
        private async void Like_Tapped(object sender, EventArgs e)
        {
            var param = ((TappedEventArgs)e).Parameter.ToString();
            Console.WriteLine(likesCount1.Text.GetType());
            Xamarin.Forms.Image likeImage = null;
            Xamarin.Forms.Span likesCount = null;
            int intLikesCount = 0;
            switch (param)
            {
                case "1":
                    likeImage = likeImage1;
                    likesCount = likesCount1;
                    break;
            }
            if (likesCount.Text == null)
            {
                likesCount.Text = "0";
            }
            if (likeImage.Source.ToString() == "File: like")
            {
                likeImage.Source = "like1";
                intLikesCount = Int32.Parse(likesCount.Text) + 1;

                await Utils.HttpPost($"/api/post/{_postId}/like", new { });
            }
            else
            {
                likeImage.Source = "like";
                intLikesCount = Int32.Parse(likesCount.Text) - 1;

                await Utils.HttpPost($"/api/post/{_postId}/unlike", new { });
            }
            likesCount.Text = intLikesCount.ToString();
        }

        private void Comment_Tapped(object sender, EventArgs e)
        {

        }
        private async void Username_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new OtherUserPage(_username));
        }

        private async void Delete_Post(object sender, EventArgs e)
        {
            await Utils.HttpDelete($"/api/post/{_postId}");
            await Navigation.PushAsync(new ProfilePage());
        }
    }
}