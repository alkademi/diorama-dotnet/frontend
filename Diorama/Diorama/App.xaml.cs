﻿using Xamarin.Forms;
using System;

namespace Diorama
{
    public partial class App : Application
    {
        public static readonly string BASE_ADDRESS = "http://20.248.186.13";
        public static readonly string LOCAL_FS_HOST = "http://192.168.0.103:3030";
        public static readonly string IMGBB_API_KEY = "4e3c2dfa2b54b94912ed9d597c2ff1fb";

        public App()
        {
            try
            {
                InitializeComponent();
                var token = Utils.getToken();
                if (token.Equals(string.Empty))
                {
                    MainPage = new NavigationPage(new LoginUI())
                    {
                        BarBackgroundColor = Color.LightGray,
                        BarTextColor = Color.Black,
                    };
                }
                else
                {
                    MainPage = new NavigationPage(new HomePage())
                    {
                        BarBackgroundColor = Color.RoyalBlue,
                        BarTextColor = Color.White,
                    };
                }
            } catch (NullReferenceException ex)
            {
                onError();
                Console.WriteLine(ex);
                MainPage = new NavigationPage(new LoginUI())
                {
                    BarBackgroundColor = Color.LightGray,
                    BarTextColor = Color.Black,
                };
            }
        }

        private async void onError()
        {
            await Storage.ClearAll();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
