﻿using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UploadPage : ContentPage
    {
        private string PhotoSource { get; set; } = string.Empty;

        public UploadPage()
        {
            InitializeComponent();
        }

        private async void ToUploadSecondPart(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(PhotoSource))
            {
                await DisplayAlert("Alert", "Please Select a Photo", "OK");
                return;
            }
            await PageUtils.Move(this, new UploadSecondPartPage(PhotoSource));
        }

        private async void PickButton_Clicked(object sender, EventArgs e)
        {
            Func<bool> onPex = delegate ()
            {
                DisplayAlert("Alert", "Permission problem", "Try Again");
                return true;
            };

            Func<bool> onUex = delegate ()
            {
                DisplayAlert("Alert", "Something went wrong", "Try Again");
                return true;
            };
            string photoRaw = await PictureUtils.PickPhotoAsync(onPex, onUex);
            await Process(photoRaw);
        }

        private async void TakeButton_Clicked(object sender, EventArgs e)
        {
            Func<bool> onPex = delegate ()
            {
                DisplayAlert("Alert", "Permission problem", "Try Again");
                return true;
            };

            Func<bool> onUex = delegate ()
            {
                DisplayAlert("Alert", "Something went wrong", "Try Again");
                return true;
            };
            string photoRaw = await PictureUtils.TakePhotoAsync(onPex, onUex);
            await Process(photoRaw);
        }

        private async Task Process(string photoRaw)
        {
            if (!string.IsNullOrWhiteSpace(photoRaw))
            {
                ImageViewer.Source = PictureUtils.ConvertB64ToImageSource(photoRaw);
                await ImageViewer.ScaleTo(1);
                PhotoSource = photoRaw;
            }
        }
        private async void Profile_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }
        private async void Home_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new HomePage());
        }
        private async void Search_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPage());
        }
        private async void Add_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }
    }
}