﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : ContentPage
    {
        public SearchPage()
        {
            InitializeComponent();
        }


        private int lastIdx = -1;
        private int lastRow = -1;

        private void Clear()
        {
            GridExplore.Children.Clear();
            lastRow = -1;
            lastIdx = -1;
        }

        private void CellFactory(UserPost data)
        {
            var tapGesRec = new TapGestureRecognizer();
            tapGesRec.Tapped += Post_Tapped;
            tapGesRec.CommandParameter = data.ID;

            var result = new StackLayout();
            result.BackgroundColor = Color.Black;
            result.HorizontalOptions = LayoutOptions.FillAndExpand;
            result.VerticalOptions = LayoutOptions.FillAndExpand;

            lastIdx++;
            lastIdx = lastIdx % 4;
            if (lastIdx == 0)
            {
                lastRow++;
            }
            if (lastIdx != 0)
            {
                Grid.SetColumn(result, lastIdx);
            }
            if (lastRow != 0)
            {
                Grid.SetRow(result, lastRow);
            }

            var img = Utils.getImageFromUrl(data.ImageUrl);
            var imgCell = new Image();
            imgCell.Aspect = Aspect.AspectFill;
            imgCell.VerticalOptions = LayoutOptions.FillAndExpand;
            imgCell.HorizontalOptions = LayoutOptions.FillAndExpand;
            imgCell.Source = img;
            result.Children.Add(imgCell);
            result.GestureRecognizers.Add(tapGesRec);

            GridExplore.Children.Add(result);
        }

        protected override async void OnAppearing()
        {
            Clear();

            var result2 = await Utils.HttpGet("/api/post/explore");
            var okobj2 = Utils.getOkObject(result2);
            if (okobj2 == null)
            {
                return;
            }
            var postsobj = (IEnumerable<object>)okobj2["posts"];
            var posts = postsobj.Select(u => (Dictionary<string, object>)u).ToList();
            NoPost.IsVisible = posts.Count <= 0;

            if (posts.Count <= 0) return;
            posts.ForEach(u =>
            {
                CellFactory(new UserPost(u));
            });
        }

        private async void Home_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new HomePage());
        }
        private async void Search_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPage());
        }
        private async void Add_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }
        private async void User_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }
        private async void Edit_Button_Clicked(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new EditProfilePage());
        }

        private async void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPageResult(SearchBarText.Text));
        }

        private async void Post_Tapped(object sender, EventArgs e)
        {
            if (sender == null)
            {
                return;
            }
            StackLayout cell = (StackLayout)sender;
            if (cell.GestureRecognizers.Count <= 0)
            {
                return;
            }
            var param = ((TapGestureRecognizer)cell.GestureRecognizers[0]).CommandParameter;
            if (param == null)
            {
                return;
            }
            await PageUtils.Move(this, new SpesificPostPage(int.Parse(param.ToString())));
        }
    }
}