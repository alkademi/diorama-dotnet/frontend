﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginUI : ContentPage
    {
        public LoginUI()
        {
            InitializeComponent();
        }

        private void FormClear()
        {
            FPassword.Text = string.Empty;
        }

        private async void Login_Button_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FUsername.Text) || string.IsNullOrWhiteSpace(FPassword.Text))
            {
                await DisplayAlert("Alert", "Please fill all the required form!", "OK");
                return;
            }
            var response = await Utils.HttpPost("/api/user/login", new { Username = FUsername.Text, Password = FPassword.Text });
            FormClear();
            var error = Utils.getErrorMessage(response);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }
            var okobj = Utils.getOkObject(response);
            await Storage.Set("Token", okobj["token"]);
            await PageUtils.Move(this, new HomePage());
        }

        private async void Signup_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new RegisterPage());
        }
    }
}