﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void FormClear()
        {
            FUsername.Text = string.Empty;
            FName.Text = string.Empty;
            FPassword.Text = string.Empty;
        }

        private async void Signup_Button_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FUsername.Text) || string.IsNullOrWhiteSpace(FPassword.Text) || string.IsNullOrWhiteSpace(FName.Text))
            {
                await DisplayAlert("Alert", "Please fill all the required form!", "OK");
                return;
            }
            var response = await Utils.HttpPost("/api/user/register", new { Name = FName.Text, Username = FUsername.Text, Password = FPassword.Text });
            FormClear();
            var error = Utils.getErrorMessage(response);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }
            await DisplayAlert("Alert", "Register Success!", "Next");
            await Navigation.PopToRootAsync();
            await PageUtils.Move(this, new LoginUI());
        }
    }
}