﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    public class UserPost
    {
        public int ID { get; set; }
        public string ImageUrl { get; set; }

        public UserPost() { }

        public UserPost(Dictionary<string, object> data)
        {
            ID = int.Parse(data["id"].ToString());
            ImageUrl = data["image"].ToString();
        }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
        }

        private int lastIdx = -1;
        private int lastRow = -1;

        private void Clear()
        {
            GridPost.Children.Clear();
            lastRow = -1;
            lastIdx = -1;
        }

        private void CellFactory(UserPost data)
        {
            var tapGesRec = new TapGestureRecognizer();
            tapGesRec.Tapped += Post_Tapped;
            tapGesRec.CommandParameter = data.ID;

            var result = new StackLayout();
            result.BackgroundColor = Color.Black;
            result.HorizontalOptions = LayoutOptions.FillAndExpand;
            result.VerticalOptions = LayoutOptions.FillAndExpand;

            lastIdx++;
            lastIdx = lastIdx % 4;
            if (lastIdx == 0)
            {
                lastRow++;
            }
            if (lastIdx != 0)
            {
                Grid.SetColumn(result, lastIdx);
            }
            if (lastRow != 0)
            {
                Grid.SetRow(result, lastRow);
            }

            var img = Utils.getImageFromUrl(data.ImageUrl);
            var imgCell = new Image();
            imgCell.Aspect = Aspect.AspectFill;
            imgCell.VerticalOptions = LayoutOptions.FillAndExpand;
            imgCell.HorizontalOptions = LayoutOptions.FillAndExpand;
            imgCell.Source = img;
            result.Children.Add(imgCell);
            result.GestureRecognizers.Add(tapGesRec);

            GridPost.Children.Add(result);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var response = await Utils.HttpGet("/api/user");
            var error = Utils.getErrorMessage(response);
            if (!string.IsNullOrEmpty(error))
            {
                await DisplayAlert("Error", error, "Try Again");
                return;
            }

            var okobj = Utils.getOkObject(response);

            username.Text = okobj["username"].ToString();
            if (!string.IsNullOrWhiteSpace(okobj["biography"].ToString()))
            {
                biography.Text = okobj["biography"].ToString();
            }
            else
            {
                biography.Text = "Available";
            }
            totalFollowers.Text = okobj["followers"].ToString();
            totalFollowing.Text = okobj["following"].ToString();
            name.Text = okobj["name"].ToString();

            // Setup post image.
            var pictureSource = Utils.getImageFromUrl(okobj["profile_picture"].ToString());
            profilePicture.Source = pictureSource;

            Clear();

            var result2 = await Utils.HttpGet("/api/post/mine");
            var okobj2 = Utils.getOkObject(result2);
            if (okobj2 == null)
            {
                return;
            }
            var postsobj = (IEnumerable<object>)okobj2["posts"];
            var posts = postsobj.Select(u => (Dictionary<string, object>)u).ToList();
            NoPost.IsVisible = posts.Count <= 0;

            if (posts.Count <= 0) return;
            posts.ForEach(u =>
            {
                CellFactory(new UserPost(u));
            });
        }

        public ProfilePage(string postID)
        {
            InitializeComponent();
        }
        private async void Upload_Clicked(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }

        private async void Home_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new HomePage());
        }

        private async void Search_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPage());
        }

        private async void Add_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }
        private async void Profile_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }

        private async void Post_Tapped(object sender, EventArgs e)
        {
            if (sender == null)
            {
                return;
            }
            StackLayout cell = (StackLayout)sender;
            if (cell.GestureRecognizers.Count <= 0)
            {
                return;
            }
            var param = ((TapGestureRecognizer)cell.GestureRecognizers[0]).CommandParameter;
            if (param == null)
            {
                return;
            }
            await PageUtils.Move(this, new SpesificPostPage(int.Parse(param.ToString())));
        }

        private async void Edit_Button_Clicked(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new EditProfilePage());
        }
    }
}