﻿using System;
using System.Collections.Generic;   
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diorama
{
    public class HomePagePost
    {
        public string Username { get; set; }
        public string Likes { get; set; }
        public string Caption { get; set; }
        public string PostId { get; set; }
        public UriImageSource PostImage { get; set; }

        public HomePagePost(Dictionary<string, object> data)
        {
            Dictionary<string, object> author = (Dictionary<string, object>)data["author"];
            Username = author["username"].ToString();

            Likes = data["likes"].ToString();
            Caption = data["caption"].ToString();
            PostImage = Utils.getImageFromUrl(data["image"].ToString());
            PostId = data["id"].ToString();
        }
        public HomePagePost() { }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
        }

        private void PostFactory(HomePagePost data)
        {
            var result = new StackLayout();
            result.Margin = new Thickness(0, 10, 0, 0);
            result.Padding = new Thickness(5, 0, 5, 0);
            result.HorizontalOptions = LayoutOptions.FillAndExpand;
            result.VerticalOptions = LayoutOptions.FillAndExpand;

            var usernameTapGestRec = new TapGestureRecognizer();
            var usernameLabel = new Label();
            usernameLabel.Text = data.Username;
            usernameLabel.FontAttributes = FontAttributes.Bold;
            usernameLabel.FontSize = 20;
            usernameTapGestRec.Tapped += User_Tapped;
            usernameTapGestRec.CommandParameter = data.Username;
            usernameLabel.GestureRecognizers.Add(usernameTapGestRec);

            var imageTapGestRec = new TapGestureRecognizer();
            var image = new Image();
            var imageSource = data.PostImage;
            image.Aspect = Aspect.AspectFill;
            image.VerticalOptions = LayoutOptions.FillAndExpand;
            image.HorizontalOptions = LayoutOptions.FillAndExpand;
            image.Source = imageSource;
            image.MinimumHeightRequest = 100;
            image.MinimumWidthRequest = 100;
            imageTapGestRec.Tapped += Post_Tapped;
            imageTapGestRec.CommandParameter = data.PostId;
            image.GestureRecognizers.Add(imageTapGestRec);

            var likesLabel = new Label()
            {
                FormattedText = new FormattedString()
                {
                    Spans =
                    {
                        new Span() { Text = $"{data.Likes} " },
                        new Span() { Text = "likes" }
                    }
                },
                FontSize = 15,
            };

            String captionText = data.Caption;
            String moreText = "";
            if (captionText.Length > 20)
            {
                captionText = captionText.Substring(0, 20) + "...";
                moreText = "See more";
            }

            var captionLabel = new Label()
            {
                FormattedText = new FormattedString()
                {
                    Spans =
                {
                    new Span() { Text = captionText},
                    new Span() { Text = moreText, TextColor = Color.DeepSkyBlue, FontAttributes = FontAttributes.Bold}
                }
                },
                FontSize = 15,
            };


            result.Children.Add(usernameLabel);
            result.Children.Add(image);
            result.Children.Add(likesLabel);
            result.Children.Add(captionLabel);

            stack.Children.Add(result);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (!string.IsNullOrWhiteSpace(Utils.getToken()))
            {
                RenderHomepagePosts();
            }
            else
            {
                await PageUtils.Move(this, new LoginUI());
            }
        }

        private async void RenderHomepagePosts()
        {
            var result = await Utils.HttpGet("/api/post/homepage");
            var okobj = Utils.getOkObject(result);
            var postsobj = (IEnumerable<object>)okobj["posts"];
            var posts = postsobj.Select(u => (Dictionary<string, object>)u).ToList();

            NoPost.IsVisible = posts.Count <= 0;
            if (posts.Count() <= 0) return;

            posts.ForEach(post =>
            {
                PostFactory(new HomePagePost(post));
            });
        }

        private async void Logout_Clicked(object sender, EventArgs e)
        {
            await Storage.ClearAll();
            await Navigation.PopToRootAsync();
            await PageUtils.Move(this, new NavigationPage(new LoginUI()));
        }
        private async void Profile_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new ProfilePage());
        }
        private async void Home_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new HomePage());
        }
        private async void Search_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new SearchPage());
        }
        private async void Add_Tapped(object sender, EventArgs e)
        {
            await PageUtils.Move(this, new UploadPage());
        }

        private async void User_Tapped(object sender, EventArgs e)
        {
            if (sender == null || !(sender is Label))
            {
                return;
            }
            Label cell = (Label)sender;
            var param = ((TapGestureRecognizer)cell.GestureRecognizers[0]).CommandParameter.ToString();
            await PageUtils.Move(this, new OtherUserPage(param));
        }

        private async void Post_Tapped(object sender, EventArgs e)
        {
            if (sender == null)
            {
                return;
            }
            Image cell = (Image)sender;
            var param = ((TapGestureRecognizer)cell.GestureRecognizers[0]).CommandParameter.ToString();
            await PageUtils.Move(this, new SpesificPostPage(int.Parse(param)));
        }
    }
}